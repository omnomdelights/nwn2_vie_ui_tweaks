# Vie's UI Tweaks for Neverwinter Nights 2
A redesign of the Neverwinter Nights 2 UI.

Using [Charlies UI]("https://neverwintervault.org/project/nwn2/other/gui/charlies-ui") mod as a base..

I put together what I feel is a simple ui that strives to keep all relevant information centralized

and organized in such a way that removes the need for extra windows,

thus giving you more clean space to see what is going on in the game.

These tweaks were designed with the [RoT Community](https://www.realmsoftrinity.com/forum/viewtopic.php?f=46&t=5429)
in mind and is best used with server UI modifications.

I'm always open to take suggestions and constructive criticism!

This is still a work in progress so elements of the UI are always subject to change..

!["Hotbar Image"](https://cdn.discordapp.com/attachments/279873114060619776/809334046844125224/unknown.png)

[Click Here to see the full screenshot](https://cdn.discordapp.com/attachments/279873114060619776/808714753147928577/unknown.png)


**Permissions & licensing:** Open - Free & open only if project also open. -- You should always give credit for work that isn't your own.

**Download:** [.zip](https://bitbucket.org/omnomdelights/nwn2_vie_ui_tweaks/get/0d4d0de81016.zip)

# C O M I N G  ⦿  S O O N
━━━━━━━━━━━━━━━━━━━━━━━━━

- Minimal Version -- Removes Gold / Encumbrance / Level Up Button


# F E A T U R E S
━━━━━━━━━━━━━━━━━━━━━━━━━

**Vie's UI Tweaks**

 - Added transparency
 - Increased hotbar slots from 12 to 20. (6 hotbars of 20)
     - Hotbar 1 = Slots 1-60 (3 hotbars)
     - Hotbar 2 = Slots 61-120 (3 hotbars)
     - This allows the player to have vision of all available hotbars at the same time
     
 - Both Vertical hotbars were turned horizontal and resized to 2 rows instead of 3
 - Moved Camera Modes to a vertical position
 - Resized the Vhotbars to be slightly bigger than charlie's chosen size   
 - Removed default Action Queue window
 - Removed default Player Menu
 - Attached Action Queue to top of hotbar
 - Added a level up button above camera modes
 - Added display of weight and gold
 - Added Player Menu buttons to the left side of the primary hotbar
   (Placeholder buttons will be fixed in future update)
   
   
 
**Charlies UI**

 - Side by side re-sizable multi player chat windows
 - A more compact character sheet (all pages)
 - Classic alignment value display (multi player safe).
 - Compact creature examine (same as character sheet).
 - 12 x 10 scrolling hot-bar, 33x2 big hot-bar & 2 12x1 vertical hot-bars.
 - compact, transparent voice chat with back arrow.
 - Compact player/rest menu
 - Extended action queue bar
 - Compact 2x8x8 inventory
 - Compact spell book
 - Compact stores
 - Compact party bar
 - Compact target box
 - Larger journal with compact headings.
 - Compact character selection page when loading/starting new game
 - Smaller mini-map icons and modified camera direction indicator.
 - Drag-able death screens.
 - More compact player list.
 - Double click enabled to join a server or LAN game.
 - Larger player chat selection window with a background.
 - United Colors - Greatly increases the choice of colors during character creation. - by Gaoneng
 - Compatable with Tony K's Companion and Monster AI v2.0
 
 **and for DMs**

 - Compact DM tool bar and difficulty slider with a border for easy dragging.
 - Compact & drag-able DM Quick Cast
 - Compact, single row of buttons in the DM Chooser.


 

# I N S T A L L A T I O N
━━━━━━━━━━━━━━━━━━━━━━━━━

 - Remove any old version of Vie's UI / Charlie's UI
 - Rename **guisettings.ini** in
    _"My Documents\Neverwinter Nights 2\UI\default"_ to **guisettings.bak**
    
 - Unzip the download into "My Documents\Neverwinter Nights 2\override".

    When you connect to a multiplayer server, this is where your files will download to.

    If you use the -home flag to override this setting,
    then that is where your override folder is.

    To clarify this is NOT the override folder in your game folder!
    

**N O T E S**

 - YOU **DO NOT NEED** TO DOWNLOAD **CHARLIES UI**
 - For 1.04+ hot-bars, you must restart the game after
   turning on/off hot-bars in the options->interface menu.
   
 - For the 'classic alignment value' to display in multiplayer
   the script file 'gui_vn_classic_alignment.NCS' must be
   installed in the server's override directory.
   ○ Without it you will get the standard alignment display.
   
 - (optional) If you don't want, or have conflicts with united-colors,
    simply delete that directory.
   
 - (optional) For compatibility with xUI
    rename override\vie_ui\hotbar.xml to hotbar.bak
   
-----


# B U G S
━━━━━━━━━━━━━━━━━━━━━━━━━

Will be fixed in future updates
 
 - Custom voice command slots above hotbar breaks on RoT.

-


# C R E D I T S
# & P E R M I S S I O N S

━━━━━━━━━━━━━━━━━━━━━━━━━

[VieLassiel](https://bitbucket.org/omnomdelights/) | [Vie's UI Tweaks](https://bitbucket.org/omnomdelights/nwn2_vie_ui_tweaks/src/master/)

[Charlie Vale](https://neverwintervault.org/tags/charlie-vale) | [Charlie's UI](https://neverwintervault.org/project/nwn2/other/gui/charlies-ui)

Thank you testers!

 - mica_kr
 - TheLostSock
